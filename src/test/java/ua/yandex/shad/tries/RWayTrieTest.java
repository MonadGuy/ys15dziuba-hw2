package ua.yandex.shad.tries;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

public class RWayTrieTest {
	
	static RWayTrie emptyTrie = new RWayTrie();

	@Test
	public void sizeOfEmptyTrieShouldReturnZero() {
		assertEquals(emptyTrie.size(), 0);
	}

	@Test
	public void containsWithEmptyStringOfEmptyTrieShouldReturnFalse() {
		assertEquals(emptyTrie.contains(""), false);
	}

	@Test 
	public void deleteOfEmptyTrieShouldReturnFalse() {
		assertEquals(emptyTrie.delete(""), false);
	}

	@Test
	public void wordsOfEmptyTrieShouldReturnEmptyIterable() {
		boolean isNext = emptyTrie.words().iterator().hasNext();
		assertEquals(isNext, false);
	}
}