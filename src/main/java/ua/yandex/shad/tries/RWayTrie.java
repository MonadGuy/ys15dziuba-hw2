package ua.yandex.shad.tries;

import java.util.ArrayList;

public class RWayTrie implements Trie {

    public RWayTrie() {

    }

    @Override
    public void add(Tuple t) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean contains(String word) {
        return false;
    }

    @Override
    public boolean delete(String word) {
        return false;
    }

    @Override
    public Iterable<String> words() {
        return new ArrayList();
    }

    @Override
    public Iterable<String> wordsWithPrefix(String s) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int size() {
        return 0;
    }

}
